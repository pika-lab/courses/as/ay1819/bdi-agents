// Agent fanboy in project sa1415_agentSpeakL

/* Initial beliefs */

likes("Radiohead").
phone("Glasgow Arena","05112345").
concert("Radiohead", "22/12/2014", "Glasgow Arena"). // still triggers belief addition event
//busy(phone).
//~busy(phone).

/* Initial goals */

!start.

/* Plans */

+!start : true <- .print("Hello world!").

+concert(Artist, Date, Venue) // triggering event (belief addition)
	: likes(Artist) // plan context
	<- !book_tickets(Artist, Date, Venue). // plan body

+!book_tickets(A, D, V) // triggering event (achievement goal addition)
	: not busy(phone) & not ~busy(phone) // lack of knowledge
	<- ?phone(V, N); // test goal (to retrieve a belief)
       !call(N);
       !choose_seats(A, D, V);
       !done.

+!call(N) : true <- .print("calling ", N, "...").

+!choose_seats(A, D, V) : true <- .print("choosing seats for ", A, " at ", V, "...").

+!done : true <- .print("done!").
