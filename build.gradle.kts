import de.undercouch.gradle.tasks.download.Download
import jason.util.Config
import jason.runtime.RunJasonProject

plugins {
    java
    id("de.undercouch.download") version("3.4.3")
}

buildscript {
    dependencies {
        classpath(files("libs/jason-2.4.jar"))
    }
}

repositories {
    mavenCentral()
}

group = "it.unibo.as"
version = "1.0-SNAPSHOT"

fun projectFile(relative: String): File {
    return File(rootProject.projectDir, relative)
}

fun projectPath(relative: String): String {
    return projectFile(relative).absolutePath
}

subprojects {

    repositories {
        mavenCentral()
    }

    apply<JavaPlugin>()
    /* apply<ApplicationPlugin>() */

    dependencies {
        implementation(
            files(
                projectPath("libs/jason-2.4.jar"),
                projectPath("libs/ant-1.10.5.jar"),
                projectPath("libs/ant-launcher-1.10.5")
            )
        )
        testImplementation("junit", "junit", "4.12")
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
    }

    task<JavaExec>("runJason") {
        /* dependsOn("setUpMas2J") */
        sourceSets {
            main { /* mainSourceSet -> */
                  classpath = runtimeClasspath
                  args = listOf(
                      /* allSource.filter{ it.name.endsWith(".mas2j") }.singleFile.path */
                      /* properties["mas2jFile"].toString() */
                      project.property("mas2jFile").toString() + ".mas2j"
                  )
            }
        }
        /* classpath = sourceSets.main.java.runtimeClasspath */
        main = RunJasonProject::class.java.name
        standardInput = System.`in`
        doFirst {
            println("Setting-up Ant:")
            val config = Config.get()
            config.fix();
            config.setAntLib(projectPath("libs"))
            println("AntLib: '${config.getAntLib()}'")
            println("JavaHome: '${config.getJavaHome()}'")
            config.setProperty(Config.JASON_JAR, projectPath("libs/jason-2.4.jar"))
            println("JasonJar: '${config.getJasonJar()}'")
            config.store()
            println("\tStored new Config: ${config}")
            println("Running `java -cp ${classpath!!.joinToString(" ")} ${main} ${args!!.joinToString(" ")}`...")
        }
    }
}
